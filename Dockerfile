# Use the official Golang image as the parent image
FROM golang:1.16-alpine

# Set the current working directory inside the container
WORKDIR /app

# Copy go.mod and go.sum files to the working directory
COPY go.mod go.sum ./

# Download all dependencies. They will be cached if the go.mod and go.sum files are not changed
RUN go mod download

# Copy the rest of the application source code to the working directory
COPY . .

# Copy the .env file to the working directory
COPY .env .

# Build the Go application and statically link it
RUN go build -o app

# Start the application
CMD ["./app"]