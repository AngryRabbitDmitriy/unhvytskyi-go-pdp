package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"

	"github.com/briandowns/openweathermap"
)

type Response struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

func init() {
	if err := godotenv.Load(); err != nil {
		fmt.Println("No .env file found")
	}
}

func main() {
	port := os.Getenv("PORT")

	fmt.Printf("Starting server at port %s\n", port)

	http.HandleFunc("/getWeather", getWeather)

	log.Fatal(http.ListenAndServe(":"+port, nil))
}

func getWeather(w http.ResponseWriter, r *http.Request) {
	apiKey := os.Getenv("OPENWEATHERMAP_API_KEY")
	metric := os.Getenv("OPENWEATHERMAP_METRIC")
	output := os.Getenv("OPENWEATHERMAP_OUTPUT")

	if r.Method != http.MethodPost {
		res := Response{
			Status:  http.StatusBadRequest,
			Message: "Invalid method",
		}
		json.NewEncoder(w).Encode(res)
		return
	}

	city := r.FormValue("city")
	if city == "" {
		res := Response{
			Status:  http.StatusBadRequest,
			Message: "Missing 'city' field in the body",
		}
		json.NewEncoder(w).Encode(res)
		return
	}

	weather, err := openweathermap.NewCurrent(metric, output, apiKey)
	if err != nil {
		log.Println("Error creating OpenWeatherMap client:", err)
		res := Response{
			Status:  http.StatusInternalServerError,
			Message: "Server error",
		}
		json.NewEncoder(w).Encode(res)
		return
	}

	weather.CurrentByName(city)
	if weather.Cod != 200 {
		res := Response{
			Status:  http.StatusBadRequest,
			Message: "City is not valid",
		}
		json.NewEncoder(w).Encode(res)
		return
	}

	res := Response{
		Status:  http.StatusOK,
		Message: fmt.Sprintf("Weather in %s: %.1f°%s", city, weather.Main.Temp, metric),
	}
	json.NewEncoder(w).Encode(res)
}
