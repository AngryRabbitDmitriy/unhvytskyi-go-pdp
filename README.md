# unhvytskyi-go-pdp

## Getting started

PDP Project based on using Golang and Openweathermap. 

It's simple app which can be used to get current temparature in the requested city. 

The goal for me was to become acquainted with Golang basic things, such as:

- Golang install, app init
- Server init 
- REST API
- Using .env files 
- Error handling
- Work with packages (install, import)
- Work with 3rd party API

## How to run app:

1. install go (if it's not installed yet) https://go.dev/doc/install
2. clone the project
3. at the project folder run command: "cp .env.example .env"
4. add API_KEY from https://openweathermap.org/ (you have to sign up) to OPENWEATHERMAP_API_KEY in the .env file
5. go run main.go

OR, instead of "5. go run main.go" - use Docker

5. docker build -t goapp .
6. docker run -p 3000:3000 --env-file=.env goapp

## How to use:

curl --location 'http://localhost:3000/getWeather' \
--form 'city="Kyiv"' 

Instead of Kyiv you can use any other city